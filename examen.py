import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import json


def open_file():
    try:
        with open('contactos.json', 'r') as file:
            data = json.load(file)
        file.close()
    except IOError:
        data = []
    return data
    
    
def save_file(data):
    print("Save File")

    with open('contactos.json', 'w') as file:
        json.dump(data, file, indent=4)
    file.close()
    
    
class Ventana_inicio():
	def __init__(self):

		self.builder = Gtk.Builder()
		self.builder.add_from_file("MenuTelefono.glade")
		self.window = self.builder.get_object("menu")
		self.window.connect("destroy", Gtk.main_quit)
		self.window.set_default_size (400,400)
		self.window.set_title("Menu telefonico")

		self.window.show_all()
		
		self.button_cancel = self.builder.get_object("boton_cancelar")
		self.button_cancel.connect("clicked", self.button_cancel_clicked)
	
		
		self.button_agregar = self.builder.get_object("boton_agregar")
		self.button_agregar.connect("clicked",self.button_agregar_clicked)
		
		self.button_llamar = self.builder.get_object("boton_llamar")
		self.button_llamar.connect("clicked",self.button_llamar_clicked)
		
		self.contacto = self.builder.get_object("contact")
		
		self.btn0 = self.builder.get_object("0")
		self.btn1 = self.builder.get_object("1")
		self.btn2 = self.builder.get_object("2")
		self.btn3 = self.builder.get_object("3")
		self.btn4 = self.builder.get_object("4")
		self.btn5 = self.builder.get_object("5")
		self.btn6 = self.builder.get_object("6")
		self.btn7 = self.builder.get_object("7")
		self.btn8 = self.builder.get_object("8")
		self.btn9 = self.builder.get_object("9")
		
		
		self.btn0.connect("clicked", self.intro)
		self.btn1.connect("clicked", self.intro)
		self.btn2.connect("clicked", self.intro)
		self.btn3.connect("clicked", self.intro)
		self.btn4.connect("clicked", self.intro)
		self.btn5.connect("clicked", self.intro)
		self.btn6.connect("clicked", self.intro)
		self.btn7.connect("clicked", self.intro)
		self.btn8.connect("clicked", self.intro)
		self.btn9.connect("clicked", self.intro)

		self.numeros = self.builder.get_object("numeros")
		
		

	def intro(self, btn=None):
		seleccionado = btn.get_label()
		numeros = self.numeros.get_text()
		print("se hizo click en ", seleccionado)
		self.numeros.set_text(numeros + seleccionado)

	def button_cancel_clicked(self, btn=None):
		self.numero = ("")
		self.numeros.set_text(self.numero)
	
	def button_agregar_clicked(self, btn=None):
		numero = self.numeros.get_text()
		d = Agregar(numero)
		print(numero)
		
	def button_llamar_clicked(self, btn=None):
		self.respaldo = self.numeros.get_text()
		p = open_file()
		for i in p:
			if i["numero"] == self.numeros.get_text():
				self.contacto.set_text(i["Nombre"]+"\n"+i["numero"])
			if i["numero"] == self.numeros.get_text():
				self.contacto.set_text(i["Nombre"]+"\n"+i["numero"])

class Agregar():
	
	def __init__(self,numero):
		#print(numero)
		self.numero = numero
		
		print("constructor")
		self.builder = Gtk.Builder()
		self.builder.add_from_file("MenuTelefono.glade")
		self.dialogo = self.builder.get_object("agregar_contacto")
		self.dialogo.show_all()
		
		
		self.button_cancel = self.builder.get_object("boton_cancela")
		self.button_cancel.connect("clicked", self.button_cancel_clicked)
		
		self.button_ok = self.builder.get_object("boton_aceptar")
		self.button_ok.connect("clicked", self.button_ok_clicked)

		self.entry_name = self.builder.get_object("nombre")
		self.entry_numero = self.builder.get_object("numero")
		
		self.entry_numero.set_text(numero)


	def button_ok_clicked (self, btn=None):
		nombre = self.entry_name.get_text()
		numero = self.entry_numero.get_text()
		
		j = {"Nombre": nombre,
			 "numero":numero,
			}

		f = open_file()
		f.append(j)
		save_file(f)
		self.dialogo.destroy()

	def button_cancel_clicked(self, btn=None):
		self.dialogo.destroy()
		

	

if __name__ == "__main__":
	p = Ventana_inicio()
	Gtk.main()
